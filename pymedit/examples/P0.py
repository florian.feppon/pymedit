# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import sys
import matplotlib.pyplot as plt
from pymedit import Mesh, square, P1Function, P0Function

if len(sys.argv)==3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0

plt.ion()

M = square(20,20,debug=debug)
M.plot(triIndices=True);
phi=P0Function(M,lambda x : x[1])
phi.plot();
phi2=P0Function(M,lambda x : x[0])
phi2.plot(title="phi2",cmap="gray")
phi3=phi+phi2;
phi4=phi+1;
phi3.plot(title="phi+phi2");
phi4.plot(title="phi+1");
phi5=P1Function(M,phi2);
phi5.plot(title="phi2 converted in P1");
input("Press enter to close all plots.");
plt.close('all')

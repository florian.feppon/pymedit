# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import sys
import numpy as np
import matplotlib.pyplot as plt

from pymedit import Mesh, trunc, connectedComponents, square, P1Function
from pymedit import mmg2d, mshdist, advect, P0Function, P1Vector


if len(sys.argv)>=3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0
if len(sys.argv)>=4 and sys.argv[3]=='--noplot':
    plot = False
    def show(*args, **kwargs):
        pass
    plt.show = show
else:
    plot = True
    plt.ion()

M = square(100, 100, debug=debug)
phi = P1Function(M, lambda x: np.cos(10*x[0])*np.cos(10*x[1])-0.3)
newM = mmg2d(M, 0.01, 0.03, 1.3, 0.001, sol=phi, ls=True)
Mf = trunc(newM, 3)

M.plot(title="Initial mesh")
phi.plot(title="Initial level set P0 function.")
Mf.plot(title="Truncated mesh")

regions = P0Function(newM, connectedComponents(newM))
regions.plot(title="Connected components of the new mesh obtained with mmg2d")

newM.plot(XLIM=[0.5, 0.58], YLIM=[0.49, 0.55],
          bcEdgeIndices=True, triIndices=True, nodeIndices=True,
          title="Connectivity indices")
newM.plot(XLIM=[0.5, 0.58], YLIM=[0.49, 0.55],
          bcEdgeLabels=True, triLabels=True, nodeLabels=True,
          title="Connectivity labels")

phi = mshdist(newM)
phi.plot(title="Signed distance function computed with mshdist", vmin=-0.2, 
         vmax=0.2)

theta = P1Vector(newM, lambda x : [1,0])
theta.plot(title="theta")
phiAdvected = advect(newM, phi, theta, T=0.3)
phiAdvected.plot(title="Advected level set function with advect", vmin=-0.2, 
                 vmax = 0.2)

input("Press enter to close all plots.")
plt.close('all')

# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import sys
import matplotlib.pyplot as plt
from pymedit import Mesh, square, P1Function

if len(sys.argv)==3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0

plt.ion()

M = square(40,40,debug=debug)
M.plot(title="Mesh")
phi = P1Function(M, lambda x: 2*x[0]**2+x[1]**2)
phi.plot(title="phi")
phi.dxP0().plot(title="dxphiP0")
phi.dyP0().plot(title="dyphiP0")
phi2 = P1Function(M, phi.dxP0())
phi2.plot(title="dxphiP1")

#Operations on P1Functions are supported
phi3 = (-phi/phi2+2*phi2+1)*(phi**2)
phi3.plot(title="phi3 (operations)")
phi4=P1Function(M, lambda x : min((x[0]-0.25)**2+(x[1]-0.5)**2-0.2**2,
                                  (x[0]-0.75)**2+(x[1]-0.5)**2-0.2**2));
phi4.plot(title="phi4");
input("Press enter to close all plots.");
plt.close('all')

# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import sys
import numpy as np

from pymedit import generate2DMesh, square, mmg2d, P1Function, trunc

import matplotlib.pyplot as plt
plt.ion()
if len(sys.argv)>=3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0

hmin, hmax_loc, hmax, hausd = 0.01, 0.01, 0.05, 0.00001
hgrad = 1.3
params = f"""
Parameters
6
 
1 Edges {hmin} {hmax_loc} {hausd}
2 Edges {hmin} {hmax_loc} {hausd}
3 Edges {hmin} {hmax_loc} {hausd}
4 Edges {hmin} {hmax_loc} {hausd}
20 Edges {hmin} {hmax_loc} {hausd}
21 Edges {hmin} {hmax_loc} {hausd}
"""

M = square(140, 140, transfo = lambda x,y : (x-0.5,y-0.5), debug=debug)
M.plot(title="Plotting initial mesh")

load  = lambda x : max(abs(x[1])-0.05,0.4-x[0])-1e-5
insert = lambda x : max(abs(x[1]+0.3)-0.05,x[0]+0.4)-1e-5
phi0 = lambda x : min(load(x),insert(x))

phicut = P1Function(M,phi0)
phicut.plot(title="phiCut",fill=False)
M0 = mmg2d(M,hmin,hmax,hgrad,hausd,nr=False, params=params,ls=True,sol=phicut)
M0.plot(title="Mesh with patches")
M0.edges[M0.edges[:,-1]==10,-1]=5
M0._AbstractMesh__updateBoundaries()
M0 = trunc(M0,2)


M0.plot(boundary='all')
#
epsCut = 0.1
loadeps = lambda x : (x[0]-0.4)**2+x[1]**2-0.0125
support = lambda x : np.sqrt((x[0]+0.45)**2+(x[1]+0.35)**2)-0.05
#

newM = generate2DMesh(M0, [loadeps, support], [20,21],
                      hmin, hmax, hausd, hgrad, detectCorners=True, params=params)
newM.plot(title="Generated mesh",boundary='all')
newMonlybc = mmg2d(newM,hmin,hmax,hgrad,hausd, params=params,sol=P1Function(newM,lambda x: 1),ls=True)
newMonlybc.plot(boundary='all',title="Final mesh")
input("Press enter to continue.")

# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import sys
import numpy as np

from pymedit import Mesh3D, trunc3DMesh, connectedComponents3D, cube, P1Function3D
from pymedit import mmg3d, mshdist, advect, P0Function3D, P1Vector3D, saveToVtk


if len(sys.argv)>=3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0
if len(sys.argv)>=4 and sys.argv[3]=='--noplot':
    import medit.external as ext
    def medit(*args,**kwargs):
        pass
    ext.medit = medit

M = cube(30, 30, 30, debug=debug)
M.plot("Plotting initial mesh",keys="bceZZZZ")
phi =\
    P1Function3D(M, lambda x: np.cos(10*x[0])*np.cos(10*x[1])*np.cos(10*x[2])-0.3)
newM = mmg3d(M, 0.02, 0.05, 1.3, sol=phi, ls=True)
Mf = trunc3DMesh(newM, 3)
phi.plot(title="Plotting Initial level set P0 function.",keys="bmZZZZ")
Mf.plot(title="Plotting Truncated mesh",keys="bceZZZZ")

regions = P0Function3D(newM, connectedComponents3D(newM))
regions.plot(title="Plotting Connected components of the new mesh "
                    " obtained with mmg3d",
             keys=["b","m","F1","F2","Z","Z","Z","Z"])

phi = mshdist(newM,debug=debug)
phi.plot(title="Plotting Signed distance function computed with mshdist",
         keys="bmZZZZ")

theta = P1Vector3D(newM, lambda x : [0.3,0,0])
phiAdvected = advect(newM, phi, theta, T=1,debug=debug)
phiAdvected.plot(title="Advected level set function with advect",keys="bmZZZZ")

saveToVtk(newM, [regions,phi,theta,phiAdvected],
          ["regions","phi","theta",
           "phiAdvected"], [0,1,1,1], "out.vtu")

input("Press enter to continue.")

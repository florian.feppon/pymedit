# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
from pymedit import Mesh3D, trunc3DMesh, connectedComponents3D, cube, \
    P0Function3D, saveToVtk, display
import numpy as np
import scipy as sp
import sys
if len(sys.argv)==3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0

M = cube(15, 15, 15, debug=debug)
M.plot("Plotting initial mesh",keys="bceZZZZ")

#TAG the mesh with a level set function
phi =\
    P0Function3D(M, lambda x: np.cos(10*x[0])*np.cos(10*x[1])*np.cos(10*x[2])-0.3)
newM = M.copy()
## Tag tetrahedra
newM.tetrahedra[:, -1][phi.sol < 0] = 3
newM.tetrahedra[:, -1][phi.sol >= 0] = 2
# Tag surface triangles with vectorized operations
I, J = newM.tetrahedronToTetrahedron.nonzero() # connectivity matrix between tetra
adjacentTetras = np.where(newM.tetrahedra[I,-1]!=newM.tetrahedra[J,-1])[0]
tetras1 = newM.tetrahedra[I[adjacentTetras],:-1]
tetras2 = newM.tetrahedra[J[adjacentTetras],:-1]
nt = tetras1.shape[0]
# Compute intersection using sparse matrices
I = np.repeat(np.array(range(tetras1.shape[0])),4,0)
J1 = tetras1.flatten()
J2 = tetras2.flatten()
D = np.zeros_like(I)+1
A1 = sp.sparse.csr_matrix((D, (I,J1)), shape=(tetras1.shape[0],newM.ntet))
A2 = sp.sparse.csr_matrix((D, (I,J2)), shape=(tetras1.shape[0],newM.ntet))
_, intersection = A1.multiply(A2).nonzero() 
triangles = intersection.reshape(nt,3)
triangles = np.column_stack((triangles, np.full((nt,1),10)))
newM.nt += nt
newM.triangles = np.row_stack((newM.triangles,triangles))

newM.plot("Plotting tagged mesh",keys=list("bceZZZZ")+["F1"])

Ms = trunc3DMesh(newM, 2)

Ms.plot("Plotting truncated mesh", keys="bceZZZZ")



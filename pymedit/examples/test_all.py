# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import os
from pymedit.abstract import display,colored
examples_folder = os.path.split(__file__)[0]
display("Testing all examples in "+examples_folder,color="magenta",attr="bold")
print("")
files = os.listdir(examples_folder)
files.sort()
for f in files:
    if f.endswith('.py') and f != "test_all.py":
        module = os.path.splitext(f)[0]
        display("="*80,color="magenta",attr="bold")
        display("Testing "+f,color="magenta",attr="bold")
        display("="*80,color="magenta",attr="bold")
        print("")
        try:
            exec("import pymedit.examples."+module)
        except Exception as e:
            print(e)
            display("Warning, testing of "+f+" failed.",color="red",attr="bold")
        input(colored("\nEnd of the test. Press any key to continue.",color="magenta",attr="bold"))
        display("\n\n")


# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import sys
import numpy as np

from pymedit import generate3DMesh, cube


if len(sys.argv)>=3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0
if len(sys.argv)>=4 and sys.argv[3]=='--noplot':
    import medit.external as ext
    def medit(*args,**kwargs):
        pass
    ext.medit = medit

M = cube(40, 20, 20, transfo=lambda x,y,z: (2*x,y,z), debug=debug)
M.plot("Plotting initial mesh", keys="bce")

epsCut = 0.3
def petitcube1(x): return max(x[0]-0.05, x[1]-epsCut, x[2]-epsCut)


def petitcube2(x): return max(x[0]-0.05, 1-epsCut-x[1], x[2]-epsCut)


def petitcube3(x): return max(x[0]-0.05, 1-epsCut-x[1], 1-epsCut-x[2])


def petitcube4(x): return max(x[0]-0.05, x[1]-epsCut, 1-epsCut-x[2])


def attache(x): return max(1.95-x[0], np.sqrt((x[1]-0.5)**2+(x[2]-0.5)**2)-0.1)

hmin, hmax_loc, hmax, hausd = 0.05, 0.05, 0.1, 0.005
hgrad = 1.3
params = f"""
Parameters
5
 
10 Triangles {hmin} {hmax_loc} {hausd}
20 Triangles {hmin} {hmax_loc} {hausd}
21 Triangles {hmin} {hmax_loc} {hausd}
22 Triangles {hmin} {hmax_loc} {hausd}
23 Triangles {hmin} {hmax_loc} {hausd}
"""
newM = generate3DMesh(M, [petitcube1, petitcube2, petitcube3, petitcube4, attache],
                      [20,21,22,23,24], hmin, hmax, hausd, hgrad, params=params)
newM.plot("Generated mesh", keys="bce")
input("Press enter to continue.")

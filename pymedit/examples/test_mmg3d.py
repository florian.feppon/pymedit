# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
from pymedit import P1Function3D, mmg3d, Mesh3D, cube, trunc3DMesh
import numpy as np

N = 10

c = 0.2
M = cube(N, N, N, lambda x, y, z :  [x-0.5, y-0.5, z-0.5])


def phi(x): return np.abs(x[0])+np.abs(x[1])+np.abs(x[2])-c


def pphi(x):
    res = 1
    for i in range(-2, 3):
        for j in range(-2,3):
            for k in range(-2,3):
                vect = np.array([i, j, k, 0])*2*c
                res = np.minimum(res, phi(x-vect))
    return res


p = P1Function3D(M, pphi)

hmin = 0.05
hmax = 0.1
hgrad = 1.3
hausd = 0.1*hmin
Mnew = mmg3d(M, hmin, hmax, hgrad, hausd, sol=p, ls=True, nr=False, debug=10)
Ms=trunc3DMesh(Mnew, 3)
verticestotetra=Ms.verticesToTetra
connectivity = verticestotetra.T.dot(verticestotetra)

Ms.plot(keys="bce")
print(Ms.checkQuality())
Mnew.plot()

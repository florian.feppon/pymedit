# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
from pymedit import mesh3D, P1Function3D, cube, saveToVtk, P1Vector3D
import numpy as np
import scipy as sp
import sys
if len(sys.argv)==3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0
M = cube(20, 20, 20, debug=debug)
phi = P1Function3D(M, lambda x : x[0]*x[1]*x[2]);
gradphiP0x = phi.dxP0()
gradphiP0y = phi.dyP0()
gradphiP0z = phi.dzP0()
dxAnal = P1Function3D(M, lambda x: x[1]*x[2])
dyAnal = P1Function3D(M, lambda x: x[0]*x[2])
dzAnal = P1Function3D(M, lambda x: x[0]*x[1])
dxphiP1 = phi.dxP1()
dyphiP1 = phi.dyP1()
dzphiP1 = phi.dzP1()

gradphiP0x.plot(silent=True, title="Plotting dxphi P0",keys="mZZZZb")
dxphiP1.plot(silent=True, title="Plotting dxphi P1",keys="mZZZZb")

gradphiAnal = P1Vector3D(M, [dxAnal,dyAnal,dzAnal])

saveToVtk(M, [phi, [gradphiP0x, gradphiP0y, gradphiP0z],
              [dxphiP1, dyphiP1, dzphiP1],
              gradphiAnal], 
          labels=["phi","gradphiP0","gradphiP1","gradPhiAnal"],
          orders=[1,0,1,1], output="out.vtu")

# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
from pymedit import Mesh, trunc, connectedComponents, square, P0Function
import numpy as np
import sys
if len(sys.argv)==3 and sys.argv[1]=='--debug':
    debug = int(sys.argv[2])
else:
    debug = 0

M = square(30, 30,debug=debug)
phi = P0Function(M, lambda x: np.cos(10*x[0])*np.cos(10*x[1])-0.3)
newM = M.copy()
newM.triangles[:, -1][phi.sol < 0] = 3
newM.triangles[:, -1][phi.sol >= 0] = 2
Mf = trunc(newM, 3)

import matplotlib.pyplot as plt
plt.ion()
M.plot(title="Initial mesh")
phi.plot(title="Initial level set P0 function.")
Mf.plot(title="Truncated mesh")

regions = P0Function(newM, connectedComponents(newM))
regions.plot(title="Connected components")

newM.plot(XLIM=[0.5, 0.58], YLIM=[0.49, 0.55],
          bcEdgeIndices=True, triIndices=True, nodeIndices=True,
          title="Connectivity indices")
newM.plot(XLIM=[0.5, 0.58], YLIM=[0.49, 0.55],
          bcEdgeLabels=True, triLabels=True, nodeLabels=True,
          title="Connectivity labels")

input("Press enter to close all plots.");
plt.close('all')

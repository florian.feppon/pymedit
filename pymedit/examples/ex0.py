# This file is part of pymedit.
#
# pymedit is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# pymedit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# A copy of the GNU General Public License is included below.
# For further information, see <http://www.gnu.org/licenses/>.
import numpy as np
from pymedit import cube, mmg3d, P1Function3D, trunc3DMesh

M = cube(30,30,30)
M.plot("Initial mesh", keys="bceZZZZ") #keys are for setting the medit window
M.debug = 4 # For debugging and mmg3d output

# Setting a P1 level set function
phi = lambda x: np.cos(10*x[0])*np.cos(10*x[1])*np.cos(10*x[2])-0.3
phiP1 = P1Function3D(M,phi)

# Remesh according to the level set
newM = mmg3d(M, 0.02, 0.05, 1.3, sol=phiP1, ls=True) 

# Trunc the negative subdomain of the level set
Mf = trunc3DMesh(newM, 3)

Mf.plot(title="Plotting Truncated mesh",keys="bceZZZZ")

Mf.save("Thf.meshb") #Saving in binary format
